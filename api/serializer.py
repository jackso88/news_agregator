from rest_framework import serializers
from news.models import News, Categories, Sources


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Categories
        fields = ('category_id', 'category_name')


class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sources
        fields = ('source_id', 'source_name')


class NewsSerializer(serializers.ModelSerializer):

    category = CategorySerializer()
    source = SourceSerializer()

    class Meta:
        model = News
        fields = ('news_id', 'source', 'link', 'title', 'category', 'date')




