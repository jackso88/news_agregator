from rest_framework.permissions import IsAuthenticated
from api.serializer import NewsSerializer
from news.models import News
from rest_framework.viewsets import ReadOnlyModelViewSet


# Create your views here.

class NewsList(ReadOnlyModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = News.objects.all()
    serializer_class = NewsSerializer
