import api.swagger as s
from django.urls import path, include
from rest_framework import routers
from api.views import NewsList

router = routers.DefaultRouter()
router.register('news', NewsList)

urlpatterns = [
    path('api/', include(router.urls)),
    path('swagger/', s.schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
