from django.contrib import admin
from .models import News, Categories, Sources


# Register your models here.
@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ("news_id", "source_id", "link", "title", "category_id", "date")
    search_fields = ("news_id", "source_id__source_id", "link", "title", "category_id__category_id", "date")


@admin.register(Categories)
class CategoriesAdmin(admin.ModelAdmin):
    list_display = ("category_id", "category_name")
    search_fields = ("category_id", "category_name")


@admin.register(Sources)
class SourcesAdmin(admin.ModelAdmin):
    list_display = ("source_id", "source_name")
    search_fields = ("source_id", "source_name")
