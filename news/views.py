from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.generic.base import TemplateView
from news.models import News, Sources
from django.shortcuts import get_object_or_404


# Create your views here.
class NewsView(TemplateView):

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        onliner = get_object_or_404(Sources, source_name="onliner.by")
        tut = get_object_or_404(Sources, source_name="tut.by")
        context['onliner'] = News.objects.filter(source_id=onliner.source_id).order_by("-date")[:25]
        context['tut'] = News.objects.filter(source_id=tut.source_id).order_by("-date")[:20]
        return context


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(request.META['HTTP_REFERER'])


def register(request):
    if request.method != 'POST':
        form = UserCreationForm()
    else:
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            new_user = form.save()
            authenticated_user = authenticate(username=new_user.username,
                                              password=request.POST['password1'])
            login(request, authenticated_user)
            return HttpResponseRedirect(reverse('news:index'))
    context = {'form': form}
    return render(request, 'users/register.html', context)
