from django.urls import path
from django.conf.urls import url
from django.contrib.auth.views import LoginView
from . import views


urlpatterns = [
    path('', views.NewsView.as_view(), name='index'),
    url(r'^users/login/$', LoginView.as_view(template_name='users/login.html'), name='login'),
    url(r'^accounts/logout/$', views.logout_view, name='logout'),
    url(r'^register/$', views.register, name='register'),
]
