from django.db import models
from datetime import datetime


# Create your models here.
class Sources(models.Model):
    source_id = models.AutoField(primary_key=True)
    source_name = models.CharField(max_length=255)


class Categories(models.Model):
    category_id = models.AutoField(primary_key=True)
    category_name = models.CharField(max_length=255)


class News(models.Model):
    news_id = models.AutoField(primary_key=True)
    source = models.ForeignKey(Sources, on_delete=models.CASCADE, db_column='source_id', default=0)
    link = models.CharField(max_length=255)
    category = models.ForeignKey(Categories, on_delete=models.CASCADE, db_column='category_id', default=0)
    title = models.TextField()
    date = models.FloatField(default=datetime.now().timestamp())
