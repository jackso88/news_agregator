# Generated by Django 3.2.6 on 2021-08-24 07:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categories',
            fields=[
                ('category_id', models.AutoField(primary_key=True, serialize=False)),
                ('category_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Sources',
            fields=[
                ('source_id', models.AutoField(primary_key=True, serialize=False)),
                ('source_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='news',
            name='category',
            field=models.ForeignKey(db_column='category_id', default=0, on_delete=django.db.models.deletion.CASCADE, to='news.categories'),
        ),
        migrations.AlterField(
            model_name='news',
            name='source',
            field=models.ForeignKey(db_column='source_id', default=0, on_delete=django.db.models.deletion.CASCADE, to='news.sources'),
        ),
    ]
