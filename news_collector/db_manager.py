import psycopg2
import os


def upsert_to_db(incoming_data: str) -> None:
    """ Inserting data to DB """

    conn = psycopg2.connect(
        dbname=os.environ['DB_NAME'],
        user=os.environ['DB_USER'],
        password=os.environ['DB_PASSWORD'],
        host=os.environ['DB_HOST']
    )

    cursor = conn.cursor()

    sql = f"""CREATE TABLE hot_data_lake
             (
              source_name VARCHAR(255),
              link VARCHAR(255) PRIMARY KEY,
              title TEXT,
              date FLOAT,
              category VARCHAR(255)
             );

              INSERT INTO hot_data_lake
              SELECT * 
              FROM json_populate_recordset(NULL::hot_data_lake, '{incoming_data}') as js;

              INSERT INTO news_sources (source_name)
              SELECT DISTINCT hdl.source_name FROM hot_data_lake hdl
              LEFT JOIN news_sources ns ON hdl.source_name = ns.source_name 
              WHERE ns.source_name IS NULL;

              INSERT INTO news_categories (category_name)
              SELECT DISTINCT hdl.category 
              FROM hot_data_lake hdl
              LEFT JOIN news_categories nc ON hdl.category = nc.category_name 
              WHERE nc.category_name IS NULL;

              INSERT INTO news_news (link, title, date, category_id, source_id)
              (SELECT hdl.link,
                      hdl.title,
                      hdl.date,
                      nc.category_id,
                      ns.source_id
              FROM hot_data_lake hdl
              LEFT JOIN news_news nn ON nn.link = hdl.link
              INNER JOIN news_categories nc ON hdl.category = nc.category_name
              INNER JOIN news_sources ns ON ns.source_name = hdl.source_name
              WHERE nn.link IS NULL);
              
              DROP TABLE hot_data_lake;"""

    cursor.execute(sql)
    conn.commit()
    conn.close()
