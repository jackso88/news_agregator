import json
import requests
import re
from .settings import tut
from bs4 import BeautifulSoup as bs


class TutGetData:
    """ class for data collecting from TUT.by """

    def __init__(self):
        self.total_result = []

    def launch(self) -> str:

        session = requests.Session()
        response = session.get(tut.get('url'),
                               headers=tut.get('headers'))
        html = response.text
        soup = bs(html, 'html.parser')

        for element in tut.get('classes_list'):
            target_list = soup.findAll(tut.get('class_type'), {"class": element})
            pattern = tut.get('pattern')
            self.raw_data_processing(target_list, pattern)

        return json.dumps(self.total_result, ensure_ascii=False)

    def raw_data_processing(self, targets: list, pattern: list) -> None:

        for target in targets:
            link = re.findall(r'%s' % pattern[0], str(target))[0]
            date = re.findall(r'%s' % pattern[2], str(target))

            if link not in str(self.total_result) and date:
                result = {
                    'source_name': 'tut.by',
                    'link': re.findall(r'%s' % pattern[0], str(target))[0],
                    'title': re.findall(r'%s' % pattern[1], str(target))[0],
                    'date': re.findall(r'%s' % pattern[2], str(target))[0],
                    'category': re.findall(r'%s' % pattern[3], str(target))[0]
                }
                self.total_result.append(result)
