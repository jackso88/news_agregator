import requests
import re
import json
from .settings import onliner
from bs4 import BeautifulSoup as bs
from datetime import datetime
from threading import Thread, Lock


class OnlinerGetData:
    """ class for data collecting from onliner.by"""

    def __init__(self):
        self.total_res = []
        self.mutex = Lock()
        self.threads_list = []
        self.timestamp_list = []
        self.date = datetime.now().timestamp()
        self.ten_years_ago = datetime.now().timestamp() - onliner.get('required_period_timestamp')

    def launch(self) -> str:
        while self.date > self.ten_years_ago:
            self.timestamp_list.append(self.date)
            self.date -= onliner.get('one_day_timestamp')

        for th in range(len(self.timestamp_list)):
            thread = Thread(target=self.parse_content, args=(self.timestamp_list[th],))
            self.threads_list.append(thread)
            thread.start()

        for th in self.threads_list:
            th.join()

        return json.dumps(self.total_res)

    def raw_data_processor(self, target_list: list, pattern_list: list, category: str) -> None:
        for i in target_list:
            if re.findall(r'%s' % pattern_list[1], str(i)):
                result = {'source_name': 'onliner.by',
                          'link': f'https://{category}.onliner.by' + str(
                              re.findall(r'%s' % pattern_list[0], str(i))[0]),
                          'title': re.findall(r'%s' % pattern_list[1], str(i))[0],
                          'date': re.findall(r'%s' % pattern_list[2], str(i))[0],
                          'category': category,
                          }

                if result['link'] not in str(self.total_res):
                    self.mutex.acquire()
                    self.total_res.append(result)
                    self.mutex.release()

    def parse_content(self, required_date: float) -> None:
        onliner_categories = onliner.get('categories')

        for category in onliner_categories:
            headers = onliner.get('headers')
            session = requests.Session()
            response = session.get(f'https://{category}.onliner.by/?fromDate={str(required_date)}',
                                   headers=headers)
            html = response.text
            soup = bs(html, 'html.parser')

            target_list = soup.findAll(onliner.get('class_type'), {"class": onliner.get('class')})
            patterns = onliner.get('pattern')
            self.raw_data_processor(target_list, patterns, category)

            target_list_2 = soup.findAll(onliner.get('class_type'), {"class": onliner.get('class_2')})
            patterns2 = onliner.get('pattern2')
            self.raw_data_processor(target_list_2, patterns2, category)
