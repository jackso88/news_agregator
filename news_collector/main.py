import db_manager
import time
from parsers.onliner_parser import OnlinerGetData
from parsers.tut_parser import TutGetData


onliner_object = OnlinerGetData()
tut_object = TutGetData()


def run():
    db_manager.upsert_to_db(onliner_object.launch())
    db_manager.upsert_to_db(tut_object.launch())
    time.sleep(900)


if __name__ == "__main__":
    run()
